import os
import tkinter as tk
from PIL import Image, ImageTk
from image_processor import ImageProcessor

class ImageDescriber:
    def __init__(self, api_key):
        self.image_processor = ImageProcessor(api_key)
        self.folder_path = ""
        self.prompt = ""
        self.image_paths = []
        self.current_index = 0
        self.root = None
        self.image_label = None
        self.description_label = None

    def show_next_image(self):
        # Update current index and reset if it exceeds the image count
        self.current_index = (self.current_index + 1) % len(self.image_paths)

        # Load and display the next image
        image_path = self.image_paths[self.current_index]
        image_data_base64 = self.image_processor.encode_image_to_base64(image_path)
        answer = self.image_processor.image_question(prompt=self.prompt, data=image_data_base64)
        image = Image.open(image_path)
        image = image.resize((300, 300))  # Adjust size as needed
        photo = ImageTk.PhotoImage(image)
        self.image_label.config(image=photo)
        self.image_label.image = photo
        self.description_label.config(text=answer)

        # Schedule the next image update after 10 seconds
        self.root.after(10000, self.show_next_image)

    def describe_images_with_tkinter(self, folder_path, prompt):
        self.folder_path = folder_path
        self.prompt = prompt
        self.image_paths = [os.path.join(folder_path, filename) for filename in os.listdir(folder_path)
                            if filename.endswith(('.jpg', '.jpeg', '.png', '.gif'))]

        if not self.image_paths:
            print("No images found in the specified folder.")
            return

        self.root = tk.Tk()
        self.root.title("Traffic Light Classifier")

        # Create labels for image and description
        self.image_label = tk.Label(self.root)
        self.image_label.pack()
        self.description_label = tk.Label(self.root, wraplength=400)
        self.description_label.pack()

        # Start displaying images
        self.show_next_image()

        self.root.mainloop()