
**# Project Description**

A simple project which classifies which light(Red,Yellow or Green) is currently switched on in a traffic light setting

**## Usage**

The user needs to enter their Anthropic API Key( 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 )
and the program will loop through the sample_images folder (changing the image every 10 seconds) informing which light is currently on
Such a project has significant use cases in Self driving cars

**## Credits**

**claude-3-haiku-20240307** model is used to analyze the traffic light image

