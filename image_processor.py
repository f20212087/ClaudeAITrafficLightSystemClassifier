import base64
import anthropic

class ImageProcessor:
    def __init__(self, api_key):
        self.client = anthropic.Anthropic(api_key=api_key)

    def encode_image_to_base64(self, image_path):
        with open(image_path, "rb") as f:
            image_data = f.read()
        return base64.b64encode(image_data).decode("utf-8")

    def image_question(self, data, prompt="Short Description", media_type="image/jpeg", max_tokens=100):
        message = self.client.messages.create(
            model="claude-3-haiku-20240307",
            max_tokens=max_tokens,
            messages=[
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "image",
                            "source": {
                                "type": "base64",
                                "media_type": media_type,
                                "data": data,
                            },
                        },
                        {
                            "type": "text",
                            "text": prompt
                        }
                    ],
                }
            ],
        )
        return message.content[0].text
